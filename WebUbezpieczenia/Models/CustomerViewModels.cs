﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.BusinessLogic.Managers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;

namespace WebUbezpieczenia.Models
{
    public class CustomerViewModels
    {
        public int ID { get; set; }
        public string ddType { get; set; }
        public List<SelectListItem> ddTypeList { get; set; }

        [Display(Name = "Utwórz konto dla klienta")]
        public bool checkBox1 { get; set; }

        [Display(Name = "Otwórz klienta po zapisie")]
        public bool cbAfterSave { get; set; }        

        [Required]
        [Display(Name = "Login")]
        public string LoginName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Adres Email")]        
        public string Email { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierdzenie hasła")]
        //[Compare("Password", ErrorMessage = "Hasło i potwierdzenie hasła nie są identyczne.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Imię i nazwisko")]
        public string Name { get; set; }

        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        [Display(Name = "Miasto")]
        public string CustomerCity { get; set; }

        [Display(Name = "Kod pocztowy")]
        public string CustomerPostCode { get; set; }

        [Display(Name = "Ulica")]
        public string CustomerStreet { get; set; }

        [Display(Name = "Data uzyskania prawa jazdy")]
        public DateTime DrivingLicenceStartDate { get; set; }

        [Display(Name = "Zawód")]
        public string Occupation { get; set; }

        [Display(Name = "Dzieci i ich wiek")]
        public string Kids { get; set; }

        [Display(Name = "Stan cywilny")]
        public string MaritialStatus { get; set; }

        public CustomerViewModels()
        {
            this.ddTypeList = new List<SelectListItem>();
        }
    }

    public class CustomerViewSearchModel
    {
        [Required]
        [Display(Name = "Nazwa klienta (Minimum 4 znaki)")]
        public string Name { get; set; }

        public List<SelectListItem> lista { get;set;}

        public CustomerViewSearchModel()
        {
            lista = new List<SelectListItem>();
        }
    }


    public class CustomerViewEditModel : CustomerViewModels
    {
        public string ddPropertyType { get; set; }
        public List<SelectListItem> ddPropertyTypeList { get; set; }
        public IEnumerable<PropertyListModels> propertyList { get; set; }
        public IEnumerable<PolicyGridModel> policyList { get; set; }

        private DataSet analysisList;        
        private Object gvAnalysis;
        private object gvDokumenty;

        public CustomerViewEditModel() : base()
        {
            this.ddPropertyTypeList = new List<SelectListItem>();
        }

        public void FCustomer_Load(ObjectUser customer, out string ErrorMessage, int PropertyID = 0)
        {
            this.ID = customer.ID;
            ErrorMessage = "";
            using (var con = ConnectionManager.GetConnection())
            {
                CustomerManager m = new CustomerManager();
                string message;
                if (!m.LoadCustomer(con, customer, out message))
                {
                    ErrorMessage = message;
                    //BlockUI();
                    return; //stop execution if error
                }
                else
                    this.ddType = m.LoadUserType(con, this.ddTypeList, customer.CustomerTypeID).ToString();
                        //ddType.SelectedIndex = m.LoadUserType(con, ddType.Items, customer.CustomerTypeID);

                //DataSet dsAnalysis
                analysisList = m.LoadCustomersNeedsAnalysis(con, customer.ID);
                InitAnalysis(gvAnalysis, analysisList);

                var mP = new BusinessLogic.Managers.PropertyManager();
                ddPropertyType = mP.LoadPropertyType(con, ddPropertyTypeList).ToString();
                propertyList = mP.LoadPropertyList(con, customer.ID);
                InitPropertyGrid(propertyList);

                //-----------------------------------Tu 
                var pm = new PolicyManager();
                if (PropertyID == 0)
                {
                    PropertyID = propertyList.First().PropertyID;
                }
                this.policyList = pm.LoadPolicyList(con, PropertyID);
                //--------------------------------------


                StorageDocumentHelper h = new StorageDocumentHelper();
                DataSet documentsList = h.LoadDocumentsList(con, TDocumentOwner.Customer, customer.ID);
                InitDocumentsGrid(gvDokumenty, documentsList);
            }

            /*
            if (customer.RoleID == TUserRole.Customer)
            {
                tbEmail.ReadOnly = false;
                if (customer.Email.StartsWith("rand_"))
                    tbEmail.Text = string.Empty;
                else
                    tbEmail.Text = customer.Email;
            }
            else
            {
                tbEmail.ReadOnly = true;
                tbEmail.Text = customer.Email;
            }
            */
            this.Name = customer.Name;

            this.Phone = customer.Phone;

            this.CustomerCity = customer.CustomerCity;
            this.CustomerPostCode = customer.CustomerPostCode;
            this.CustomerStreet = customer.CustomerStreet;

            if (customer.CustomerData == null)
                customer.CustomerData = new ObjectUserData();
            //if (customer.CustomerData.DrivingLicenceStartDate < dtDrivingLicence.MinDate)
            //    dtDrivingLicence.Value = dtDrivingLicence.MinDate;
            //else
            //   dtDrivingLicence.Value = customer.CustomerData.DrivingLicenceStartDate;
            DrivingLicenceStartDate = customer.CustomerData.DrivingLicenceStartDate;
            this.Kids = customer.CustomerData.Kids;
            this.MaritialStatus = customer.CustomerData.MaritialStatus;
            this.Occupation = customer.CustomerData.Occupation;

            RestrictUIBasedOnClient();
        }

        private void RestrictUIBasedOnClient()
        {
            //throw new NotImplementedException();
        }

        private void InitDocumentsGrid(object gvDokumenty, DataSet documentsList)
        {
           // throw new NotImplementedException();
        }

        private void InitPropertyGrid(IEnumerable<PropertyListModels> propertyList)
        {
            //throw new NotImplementedException();
        }

        private void InitAnalysis(object gvAnalysis, DataSet analysisList)
        {
            //throw new NotImplementedException();
        }
    }
}