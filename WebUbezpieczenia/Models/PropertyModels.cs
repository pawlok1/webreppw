﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebUbezpieczenia.Models
{
    public class PropertyViewModels
    {
        public int ID { get; set; }
        public String ddPropertyType { get; set; }
        public List<SelectListItem> ddPropertyTypeList { get; set; }

        [Display(Name = "Nazwa")]
        public String tbName { get; set; }

        [Display(Name = "Numer Rejestracyjny")]
        public String tbRegNumber { get; set; }

        public String cbFuelType { get; set; }
        public List<SelectListItem> cbFuelTypeList { get; set; }

        [Display(Name = "Rodzaj nadwozia")]
        public String tbBody { get; set; }

        public string cbParkingPlace { get; set; }
        public List<SelectListItem> cbParkingPlaceList { get; set; }

        [Display(Name = "Ulica")]
        public String tbStreet { get; set; }

        [Display(Name = "Kod pocztowy")]
        public String tbPostcode { get; set; }

        [Display(Name = "Miasto")]
        public String tbCity { get; set; }

    }
    public class PropertyListModels
    {
        public String SelectLink { get; set; }

        [Display(Name ="Nazwa wys")]
        public string Nazwa { get;set;}

        [Display(Name = "Rejestracja wys")]
        public string Rejestracja { get; set; }

        [Display(Name = "Rodzaj wys")]
        public string Rodzaj { get; set; }        

        public int PropertyID { get; set; }
    }
}