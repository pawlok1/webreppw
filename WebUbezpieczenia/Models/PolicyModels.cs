﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebUbezpieczenia.Models
{
    using BusinessLogic.Objects;
    using System.ComponentModel.DataAnnotations;
    public class PolicyGridModel
    {
        public string Number { get; set; }
        public String Status { get; set; }
        public string InsurerName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
        public Decimal Cash { get; set; }
        public int ID { get;set;}
    }
}