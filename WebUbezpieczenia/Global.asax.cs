﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.Managers;

namespace WebUbezpieczenia
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ConnectionManager.InitRetryManager();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var autCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (autCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(autCookie.Value);
                if (authTicket != null && !authTicket.Expired)
                {
                    SessionHelper.fromString(authTicket.UserData);
                    //var roles = authTicket.UserData.Split(',');
                    //HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), null);
                }
            }
        }
    }
}
