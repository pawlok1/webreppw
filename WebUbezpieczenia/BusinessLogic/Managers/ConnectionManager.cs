﻿using System;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace WebUbezpieczenia.Managers
{
    public static class ConnectionManager
    {
        static public void InitRetryManager()
        {
            //this need to be moved to configuration file
            var strategy = new FixedInterval("fixed", 3, TimeSpan.FromSeconds(1), true);
            var strategies = new List<RetryStrategy> { strategy };
            var manager = new RetryManager(strategies, "fixed");
            RetryManager.SetDefault(manager);
        }

        static public ReliableSqlConnection GetConnection(String ConnectionString = "")
        {
            var policy = RetryManager.Instance.GetDefaultSqlConnectionRetryPolicy();
            if (string.IsNullOrEmpty(ConnectionString))
                
                return new ReliableSqlConnection(ConfigurationManager.ConnectionStrings["defaultDbConn"].ConnectionString, policy);
            else
                return new ReliableSqlConnection(ConnectionString, policy);
        }
    }
}