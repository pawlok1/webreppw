﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Data.SqlClient;
using WebUbezpieczenia.BusinessLogic.Objects;
using System.Data;
using WebUbezpieczenia.Managers;
using WebUbezpieczenia.BusinessLogic.Helpers;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    class AccessManager : BaseClassDbAccess, IBaseManager
    {
        private ObjectUserLogin userLogin;

        public AccessManager()
        {
        }

        public bool LoginUser(ObjectUserLogin UserLogin, out string Error)
        {
            SessionHelper.ClearSession();
            //HttpContext.Current.Session.Clear();

            this.userLogin = UserLogin;

            Error = string.Empty;
            bool result = false;

            using (var con = ConnectionManager.GetConnection())
            {
                //make sure user is valid and exists login
                if (!IsValid(con, out Error))
                    return false;

                result = LoadUser(con, out Error);
                return result;
            }
        }

        private bool LoadUser(ReliableSqlConnection con, out string Message)
        {
            //bool result = false;
            Message = string.Empty;

            if (con.State != ConnectionState.Open)
                con.Open();

            using (var com = con.CreateCommand())
            {
                com.CommandText = "spUserLoadToSession";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("ID", SqlDbType.Int)).Value = userLogin.ID;
                com.Parameters.Add(new SqlParameter("TODAY", SqlDbType.Date)).Value = DateTime.Today.ToUniversalTime();
                try
                {
                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            r.Read();
                            //single record only as there can be only one user
                            if (DBHelper.ToBool("ENABLED", r) == false)
                                Message += "Użytkownik jest nieaktywny." + Program.separator;
                            else
                                SessionHelper.Load(userLogin.ID, r);
                        }
                        else
                            Message += "Użytkownik nie został znaleziony. Upewnij się że nazwa i hasło są prawidłowe." + Program.separator;
                    }
                }
                catch (Exception e)
                {
                    Message += e.Message;
                }
            }

            return string.IsNullOrEmpty(Message);
        }

        bool IsValid(ReliableSqlConnection con, out string Message)
        {
            if (!base.Validate((object)this.userLogin, out Message))
                return false;

            if (string.IsNullOrEmpty(userLogin.Name))
                Message += "Błędna nazwa użytkownika. Brak wartości." + Program.separator;

            if (string.IsNullOrEmpty(userLogin.Password))
                Message += "Błęde hasło użytkownika. Brak wartości." + Program.separator;

            if (!string.IsNullOrEmpty(Message))
                return false;

            if (con.State != ConnectionState.Open)
                con.Open();
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spUserTestIfExists";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("LOGIN_NAME", SqlDbType.NVarChar, 60)).Value = userLogin.Name;
                com.Parameters.Add(new SqlParameter("EMAIL", SqlDbType.VarChar, 256)).Value = userLogin.Name;
                try
                {
                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            r.Read();
                            string hash = DBHelper.ToString("PASS", r);
                            EncriptionHelper h = new EncriptionHelper();
                            if (!h.ValidateString(userLogin.Password, hash))
                                Message += "Użytkownik nie został znaleziony. Upewnij się że nazwa i hasło są prawidłowe." + Program.separator;
                            else
                            {
                                userLogin.ID = DBHelper.ToInt("ID", r);
                                userLogin.ClearPassword();
                            }
                        }
                        else
                            Message += "Użytkownik nie został znaleziony. Upewnij się że nazwa i hasło są prawidłowe." + Program.separator;
                    }
                }
                catch (Exception e)
                {
                    Message += e.Message;
                }
            }

            return string.IsNullOrEmpty(Message);
        }
    }
}