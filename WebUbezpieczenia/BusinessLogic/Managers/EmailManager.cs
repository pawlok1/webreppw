﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Helpers;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    static class EmailManager
    {
        public static bool SendEmail(ReliableSqlConnection con, int RecipientID, string Body, string Subject, out string Message)
        {
            bool result = true;
            Message = string.Empty;
            try
            {
                //string subject = Subject.Replace('\r', ' ').Replace('\n', ' ');
                //string body = Body.Replace('\r', ' ').Replace('\n', ' ');

                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();

                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spUserHistoryLog";
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID; //sender
                    com.Parameters.Add(new SqlParameter("OPERATION", System.Data.SqlDbType.NVarChar, 30)).Value = "Email";
                    com.Parameters.Add(new SqlParameter("ACTION", System.Data.SqlDbType.NVarChar, 30)).Value = "W kolejce";

                    com.Parameters.Add(new SqlParameter("DATA", System.Data.SqlDbType.NVarChar)).Value = Body;
                    com.Parameters.Add(new SqlParameter("TABLE", System.Data.SqlDbType.VarChar, 30)).Value = "";
                    com.Parameters.Add(new SqlParameter("REF_RECORD_ID", System.Data.SqlDbType.Int)).Value = RecipientID; //who should receive the message
                    com.Parameters.Add(new SqlParameter("CREATED", System.Data.SqlDbType.DateTime)).Value = DateTime.Now.ToUniversalTime();
                    com.Parameters.Add(new SqlParameter("DATA2", System.Data.SqlDbType.NVarChar, 100)).Value = Subject;
                    int ID = (int)com.ExecuteScalarWithRetry();

                    QueueHelper h = new QueueHelper();
                    h.EmailAddToQueue(ID);
                }
            }
            catch (Exception e)
            {
                Message = e.Message;
                result = false;
            }

            return result;
        }
    }

}