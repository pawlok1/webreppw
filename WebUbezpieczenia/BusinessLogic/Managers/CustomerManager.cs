﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    class CustomerManager : BaseClassDbAccess
    {
        public DataSet LoadCustomersList()
        {
            DataSet result = new DataSet("customers");

            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spCustomersList";
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("AGENT_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.agentID;
                    SqlDataAdapter da = new SqlDataAdapter(com);
                    da.Fill(result);
                }
            }
            return result;
        }

        /// <summary>
        /// only for testing do not use in production 
        /// </summary>
        /// <param name="Question"></param>
        public void TestOnlyAddNewNeedAnalysisQuestion(string Question)
        {
            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spTestNeedsAnalysisAdd";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("QUESTION", System.Data.SqlDbType.NVarChar, 512)).Value = Question;
                    com.ExecuteNonQueryWithRetry();
                }
            }
        }

        public void SaveCustomerNeedsAnalysis(DataSet Analysis, int CustomerID)
        {
            var ds = Analysis.GetChanges();
            if (ds == null)
                return;
            if (Analysis.Tables[0].Rows.Count == 0)
                return;

            DataTable changes = new DataTable();
            changes.Columns.Add("ID", typeof(int));
            changes.Columns.Add("VALUE", typeof(bool));
            foreach (DataRow item in Analysis.Tables[0].Rows)
                changes.Rows.Add((int)item["ID"], (bool)item["VALUE"]);

            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                var t = con.BeginTransaction();
                using (var com = con.CreateCommand())
                {
                    com.Transaction = (SqlTransaction)t;
                    com.CommandText = "spCustomerNeedsAnalysisUpdate";
                    com.CommandType = System.Data.CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("CUSTOMER_ID", SqlDbType.Int)).Value = CustomerID;
                    com.Parameters.Add(new SqlParameter("CHANGES", SqlDbType.Structured)).Value = changes;
                    com.Parameters["CHANGES"].TypeName = "UserAnalysisType";
                    try
                    {
                        com.ExecuteNonQueryWithRetry();
                        t.Commit();
                    }
                    catch (Exception)
                    {
                        t.Rollback();
                        throw;
                    }
                }
            }
        }

        public DataSet LoadCustomersNeedsAnalysis(ReliableSqlConnection con, int CustomerID)
        {
            DataSet result = new DataSet("customer_needs");
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spCustomerNeedsAnalysisList";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                com.Parameters.Add(new SqlParameter("CUSTOMER_ID", SqlDbType.Int)).Value = CustomerID;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(result);
            }
            return result;
        }

        public bool LoadCustomer(ReliableSqlConnection con, ObjectUser o, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spCustomerGet";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("CUSTOMER_ID", SqlDbType.Int)).Value = o.ID;
                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            while (r.Read())
                            {
                                o.Name = DBHelper.ToString("NAME", r);
                                o.Email = DBHelper.ToString("EMAIL", r);
                                o.Phone = DBHelper.ToString("PHONE", r);
                                o.Enabled = DBHelper.ToBool("ENABLED", r);
                                o.Key = DBHelper.ToGuid("KEY", r);
                                o.CustomerTypeID = DBHelper.ToInt16("USER_TYPE_ID", r);
                                o.CustomerCity = DBHelper.ToString("ADDRESS_CITY", r);
                                o.CustomerPostCode = DBHelper.ToString("ADDRESS_POSTCODE", r);
                                o.CustomerStreet = DBHelper.ToString("ADDRESS_STREET", r);
                                o.CustomerDetails = DBHelper.ToString("USER_DATA", r);
                                o.RoleID = TUserRole.Customer;
                            }
                            result = true;
                        }
                        else
                            Message += "Klient nie został znaleziony";
                    }
                }
            }
            catch (Exception e)
            {
                Message += e.Message + Program.separator;
            }

            return result;
        }

        public int LoadUserType(ReliableSqlConnection con, List<SelectListItem> Items, Int16 SelectedValue = -1)
        {
            return LoadComboBox(con, Items, SelectedValue, "spUserTypeList");
        }

        public int LoadInsurers(ReliableSqlConnection con, List<SelectListItem> Items, Int16 SelectedValue = -1)
        {
            return LoadComboBox(con, Items, SelectedValue, "spInsurerList");
        }
    }
}