﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;
using WebUbezpieczenia.Models;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    class PolicyManager : BaseClassDbAccess, IBaseManager
    {
        private ObjectPolicy policy;

        public PolicyManager(ObjectPolicy Policy = null)
        {
            if (Policy != null)
                policy = Policy;
        }

        public bool EditInstalment(ReliableSqlConnection con, int PolicyID, ObjectInstalment Instalment, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spPolicyInstalmentUpdate";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = PolicyID;
                    com.Parameters.Add(new SqlParameter("INSTALMENT_ID", SqlDbType.Int)).Value = Instalment.ID;
                    com.Parameters.Add(new SqlParameter("AMOUNT", SqlDbType.Decimal)).Value = Instalment.Amount;
                    com.Parameters.Add(new SqlParameter("PAYMENT_DATE", SqlDbType.DateTime)).Value = Instalment.PaymentDate;
                    com.Parameters.Add(new SqlParameter("NOTES", SqlDbType.NVarChar)).Value = Instalment.Notes;
                    com.Parameters.Add(new SqlParameter("DATE", System.Data.SqlDbType.Date)).Value = DateTime.Now.ToUniversalTime();

                    try
                    {
                        int value = (int)com.ExecuteScalarWithRetry();
                        if (value > 0)
                        {
                            //t.Commit();
                            //policy.ID = value;
                            result = true;
                        }
                        else if (value == -1)
                            Message += "Brak dostępu";
                        else
                            Message += string.Format("Nieznany błąd: {0}", value);
                    }
                    catch (Exception e)
                    {
                        //t.Rollback();
                        Message += e.Message + Program.separator;
                    }
                }
            }
            catch (Exception e)
            {
                Message += e.Message + Program.separator;
            }

            return result;
        }

        public bool PayInstalment(ReliableSqlConnection con, int PolicyID, int InstalmentID, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spPolicyInstalmentPay";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = PolicyID;
                    com.Parameters.Add(new SqlParameter("INSTALMENT_ID", SqlDbType.Int)).Value = InstalmentID;
                    com.Parameters.Add(new SqlParameter("DATE", System.Data.SqlDbType.Date)).Value = DateTime.Now.ToUniversalTime();

                    try
                    {
                        int value = (int)com.ExecuteScalarWithRetry();
                        if (value > 0)
                        {
                            //t.Commit();
                            //policy.ID = value;
                            result = true;
                        }
                        else if (value == -1)
                            Message += "Brak dostępu";
                        else
                            Message += string.Format("Nieznany błąd: {0}", value);
                    }
                    catch (Exception e)
                    {
                        //t.Rollback();
                        Message += e.Message + Program.separator;
                    }
                }
            }
            catch (Exception e)
            {
                Message += e.Message + Program.separator;
            }

            return result;
        }


        public IEnumerable<PolicyGridModel> LoadPolicyList(ReliableSqlConnection con, int PropertyID)
        {
            List<PolicyGridModel> ret = new List<PolicyGridModel>();
            DataSet result = new DataSet("property");
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spCustomerPropertyPolicyList";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;
                com.Parameters.Add(new SqlParameter("PROPERTY_ID", System.Data.SqlDbType.Int)).Value = PropertyID;
                com.Parameters.Add(new SqlParameter("TODAY", System.Data.SqlDbType.Date)).Value = DateTime.Today;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(result);
                foreach(DataRow dr in result.Tables[0].Rows)
                {
                    PolicyGridModel pm = new PolicyGridModel();
                    pm.ID = int.Parse(dr[0].ToString());
                    pm.Number = dr[1].ToString();
                    pm.Status = dr[2].ToString();
                    pm.InsurerName = dr[3].ToString();
                    pm.CreationDate = DateTime.Parse(dr[4].ToString());
                    pm.StartDate = DateTime.Parse(dr[5].ToString());
                    pm.StopDate = DateTime.Parse(dr[6].ToString());
                    pm.Cash = Decimal.Parse(dr[7].ToString());
                    ret.Add(pm);
                }
            }
            return ret;
        }

        public bool LoadPolicy(ReliableSqlConnection con, ObjectPolicy o, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spPolicyGet";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = o.ID;
                    com.Parameters.Add(new SqlParameter("IS_RENEWAL", SqlDbType.Bit)).Value = true;

                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            while (r.Read())
                            {
                                o.Number = DBHelper.ToString("NUMBER", r);
                                o.InsurerID = DBHelper.ToInt16("INSURER_ID", r);
                                o.InsurerName = DBHelper.ToString("INSURER_NAME", r);
                                o.CreationDate = DBHelper.ToDate("CREATION_DATE", r);
                                o.StartDate = DBHelper.ToDate("START_DATE", r);
                                o.EndDate = DBHelper.ToDate("END_DATE", r);
                                o.Instalment0 = DBHelper.ToCurr("INSTALMENT0", r);
                                o.Instalment1 = DBHelper.ToCurr("INSTALMENT1", r);
                                o.Instalment2 = DBHelper.ToCurr("INSTALMENT2", r);
                                o.Instalment3 = DBHelper.ToCurr("INSTALMENT3", r);
                                o.Instalment4 = DBHelper.ToCurr("INSTALMENT4", r);
                                o.Cash = DBHelper.ToCurr("CASH", r);
                                o.Notes = DBHelper.ToString("NOTES", r);

                                o.PropertyID = DBHelper.ToInt("PROPERTY_ID", r);
                                o.CustomerKey = DBHelper.ToGuid("CUSTOMER_KEY", r);
                            }
                            result = true;
                        }
                        else
                            Message += "Obiekt nie został znaleziony";
                    }
                }
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spPolicyInstalmentList";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = o.ID;
                    if (o.Instalments == null)
                        o.Instalments = new List<ObjectInstalment>();
                    else
                        o.Instalments.Clear();
                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            while (r.Read())
                            {
                                var item = new ObjectInstalment();
                                item.ID = DBHelper.ToInt("ID", r);
                                item.Number = DBHelper.ToInt16("NUMBER", r);
                                item.Amount = DBHelper.ToCurr("AMOUNT", r);
                                item.PaymentDate = DBHelper.ToDate("PAYMENT_DATE", r);
                                item.Paid = DBHelper.ToBool("PAID", r);
                                item.Notes = DBHelper.ToString("NOTES", r);
                                o.Instalments.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Message += e.Message + Program.separator;
            }

            return result;
        }

        public bool Create(out string Message)
        {
            if (!IsValid(out Message))
                return false;

            bool result = false;

            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                using (var com = con.CreateCommand())
                {
                    SqlTransaction t = (SqlTransaction)con.BeginTransaction();
                    com.Transaction = t;
                    if (policy.ID > 0)
                    {
                        com.CommandText = "spPolicyUpdate";
                        com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = policy.ID;
                        com.Parameters.Add(new SqlParameter("TODAY", SqlDbType.Date)).Value = DateTime.Today.ToUniversalTime();
                    }
                    else
                    {
                        com.CommandText = "spPolicyAdd";
                        com.Parameters.Add(new SqlParameter("INSURER_ID", SqlDbType.SmallInt)).Value = policy.InsurerID;
                        com.Parameters.Add(new SqlParameter("CREATION_DATE", SqlDbType.Date)).Value = policy.CreationDate;
                        com.Parameters.Add(new SqlParameter("PROPERTY_ID", SqlDbType.Int)).Value = policy.PropertyID;

                        //add instalments
                        DataTable dtInstalments = new DataTable();
                        dtInstalments.Columns.Add("ID", typeof(int));
                        dtInstalments.Columns.Add("NUMBER", typeof(short));
                        dtInstalments.Columns.Add("AMOUNT", typeof(decimal));
                        dtInstalments.Columns.Add("PAYMENT_DATE", typeof(DateTime));
                        dtInstalments.Columns.Add("PAID", typeof(bool));
                        dtInstalments.Columns.Add("NOTES", typeof(string));
                        foreach (var item in this.policy.Instalments)
                            dtInstalments.Rows.Add(item.ID, item.Number, item.Amount, item.PaymentDate, item.Paid, item.Notes);

                        com.Parameters.Add(new SqlParameter("INSTALMENTS", SqlDbType.Structured)).Value = dtInstalments;
                        com.Parameters["INSTALMENTS"].TypeName = "dbo.InstalmentListType";


                        if (policy.PrevPolicyID > 0)
                            com.Parameters.Add(new SqlParameter("PREVIOUS_POLICY_ID", SqlDbType.Int)).Value = policy.PrevPolicyID;
                    }
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("NUMBER", SqlDbType.VarChar, 30)).Value = policy.Number;
                    com.Parameters.Add(new SqlParameter("START_DATE", SqlDbType.Date)).Value = policy.StartDate;
                    com.Parameters.Add(new SqlParameter("END_DATE", SqlDbType.Date)).Value = policy.EndDate;
                    com.Parameters.Add(new SqlParameter("CASH", SqlDbType.Decimal)).Value = policy.Cash;
                    com.Parameters.Add(new SqlParameter("NOTES", SqlDbType.NVarChar)).Value = policy.Notes;

                    /*not in use anymore*/
                    com.Parameters.Add(new SqlParameter("INSTALMENT0", SqlDbType.Decimal)).Value = policy.Instalment0;
                    com.Parameters.Add(new SqlParameter("INSTALMENT1", SqlDbType.Decimal)).Value = policy.Instalment1;
                    com.Parameters.Add(new SqlParameter("INSTALMENT2", SqlDbType.Decimal)).Value = policy.Instalment2;
                    com.Parameters.Add(new SqlParameter("INSTALMENT3", SqlDbType.Decimal)).Value = policy.Instalment3;
                    com.Parameters.Add(new SqlParameter("INSTALMENT4", SqlDbType.Decimal)).Value = policy.Instalment4;

                    try
                    {
                        int value = (int)com.ExecuteScalarWithRetry();
                        if (value > 0)
                        {
                            t.Commit();
                            policy.ID = value;

                            result = true;
                        }
                        else if (value == -1)
                            Message += "Brak dostępu";
                        else
                            Message += string.Format("Nieznany błąd: {0}", value);
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        Message += e.Message + Program.separator;
                    }
                }
            }
            return result;
        }

        bool IsValid(out string Message)
        {
            if (!base.Validate((object)this.policy, out Message))
                return false;

            //userID
            if (this.policy.PropertyID <= 0)
                Message += "Błędny obiekt ubezpieczenia. Brak wartości" + Program.separator;

            //policy number
            if (string.IsNullOrEmpty(this.policy.Number))
                Message += "Błędny numer polisy. Brak wartości." + Program.separator;

            if (this.policy.Number.Length > 30)
                Message += "Błędna numver polisy. Przekroczono wymaganą długość: 30 znaków." + Program.separator;

            //PropertyTypeID
            if (this.policy.InsurerID <= 0)
                Message += "Błędne towarzystwo ubezpieczeniowe. Brak wartości" + Program.separator;

            return string.IsNullOrEmpty(Message);
        }

        public int LoadInsurer(ReliableSqlConnection con, List<SelectListItem> Items, Int16 SelectedValue = -1)
        {
            return LoadComboBox(con, Items, SelectedValue, "spInsurerList");
        }

    }
}