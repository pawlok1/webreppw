﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    public class UserManager : BaseClassDbAccess, IBaseManager
    {
        private ObjectUser user;

        public UserManager(ObjectUser User)
        {
            this.user = User;
        }

        public bool Create(out string Message)
        {
            if (!IsValid(out Message))
                return false;

            bool result = false;

            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                using (var com = con.CreateCommand())
                {
                    SqlTransaction t = (SqlTransaction)con.BeginTransaction();
                    com.Transaction = t;
                    com.CommandType = System.Data.CommandType.StoredProcedure;

                    if (this.user.ID > 0)
                    {
                        switch (this.user.RoleID)
                        {
                            case TUserRole.Customer:
                                com.CommandText = "spCustomerUpdate";
                                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;
                                com.Parameters.Add(new SqlParameter("CUSTOMER_ID", System.Data.SqlDbType.Int)).Value = user.ID;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("ADDRESS_POSTCODE", System.Data.SqlDbType.VarChar, 10)).Value = user.CustomerPostCode;
                                com.Parameters.Add(new SqlParameter("ADDRESS_CITY", System.Data.SqlDbType.NVarChar, 40)).Value = user.CustomerCity;
                                com.Parameters.Add(new SqlParameter("ADDRESS_STREET", System.Data.SqlDbType.NVarChar, 60)).Value = user.CustomerStreet;
                                com.Parameters.Add(new SqlParameter("USER_DATA", System.Data.SqlDbType.NVarChar)).Value = user.CustomerDetails;
                                break;
                        }
                    }
                    else
                    {
                        switch (this.user.RoleID)
                        {
                            case TUserRole.User:
                                com.CommandText = "spUserAddUser";
                                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;
                                com.Parameters.Add(new SqlParameter("LOGIN_NAME", System.Data.SqlDbType.NVarChar, 60)).Value = user.LoginName;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("EMAIL", System.Data.SqlDbType.VarChar, 256)).Value = user.Email;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("PASS", System.Data.SqlDbType.NVarChar, 60)).Value = user.Password;
                                com.Parameters.Add(new SqlParameter("CREATION_TIME", System.Data.SqlDbType.DateTime)).Value = user.ValidFrom;
                                com.Parameters.Add(new SqlParameter("LINK", System.Data.SqlDbType.VarChar)).Value = user.Link;
                                com.Parameters.Add(new SqlParameter("VALID_TO", System.Data.SqlDbType.DateTime)).Value = user.ValidTo;
                                com.Parameters.Add(new SqlParameter("KEY", System.Data.SqlDbType.UniqueIdentifier)).Value = user.Key;
                                break;
                            case TUserRole.UserTrusted:
                                com.CommandText = "spUserAddUserTrusted";
                                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;

                                com.Parameters.Add(new SqlParameter("LOGIN_NAME", System.Data.SqlDbType.NVarChar, 60)).Value = user.LoginName;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("EMAIL", System.Data.SqlDbType.VarChar, 256)).Value = user.Email;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("PASS", System.Data.SqlDbType.NVarChar, 60)).Value = user.Password;
                                com.Parameters.Add(new SqlParameter("CREATION_TIME", System.Data.SqlDbType.DateTime)).Value = user.ValidFrom;
                                //com.Parameters.Add(new SqlParameter("LINK", System.Data.SqlDbType.VarChar)).Value = user.Link;
                                //com.Parameters.Add(new SqlParameter("VALID_TO", System.Data.SqlDbType.DateTime)).Value = user.ValidTo;
                                com.Parameters.Add(new SqlParameter("KEY", System.Data.SqlDbType.UniqueIdentifier)).Value = user.Key;
                                break;
                            case TUserRole.CustomerWithAccount:
                                com.CommandText = "spUserAddCustomerWithAccount";
                                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;

                                com.Parameters.Add(new SqlParameter("USER_TYPE_ID", System.Data.SqlDbType.SmallInt)).Value = user.CustomerTypeID;
                                com.Parameters.Add(new SqlParameter("ADDRESS_POSTCODE", System.Data.SqlDbType.VarChar, 10)).Value = user.CustomerPostCode;
                                com.Parameters.Add(new SqlParameter("ADDRESS_CITY", System.Data.SqlDbType.NVarChar, 40)).Value = user.CustomerCity;
                                com.Parameters.Add(new SqlParameter("ADDRESS_STREET", System.Data.SqlDbType.NVarChar, 60)).Value = user.CustomerStreet;
                                com.Parameters.Add(new SqlParameter("USER_DATA", System.Data.SqlDbType.NVarChar)).Value = user.CustomerDetails;

                                com.Parameters.Add(new SqlParameter("LOGIN_NAME", System.Data.SqlDbType.NVarChar, 60)).Value = user.LoginName;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("EMAIL", System.Data.SqlDbType.VarChar, 256)).Value = user.Email;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("PASS", System.Data.SqlDbType.NVarChar, 60)).Value = user.Password;
                                com.Parameters.Add(new SqlParameter("CREATION_TIME", System.Data.SqlDbType.DateTime)).Value = user.ValidFrom;
                                com.Parameters.Add(new SqlParameter("LINK", System.Data.SqlDbType.VarChar)).Value = user.Link;
                                com.Parameters.Add(new SqlParameter("VALID_TO", System.Data.SqlDbType.DateTime)).Value = user.ValidTo;
                                com.Parameters.Add(new SqlParameter("KEY", System.Data.SqlDbType.UniqueIdentifier)).Value = user.Key;
                                break;
                            case TUserRole.Customer:
                                com.CommandText = "spUserAddCustomer";
                                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;

                                com.Parameters.Add(new SqlParameter("USER_TYPE_ID", System.Data.SqlDbType.SmallInt)).Value = user.CustomerTypeID;
                                com.Parameters.Add(new SqlParameter("ADDRESS_POSTCODE", System.Data.SqlDbType.VarChar, 10)).Value = user.CustomerPostCode;
                                com.Parameters.Add(new SqlParameter("ADDRESS_CITY", System.Data.SqlDbType.NVarChar, 40)).Value = user.CustomerCity;
                                com.Parameters.Add(new SqlParameter("ADDRESS_STREET", System.Data.SqlDbType.NVarChar, 60)).Value = user.CustomerStreet;
                                com.Parameters.Add(new SqlParameter("USER_DATA", System.Data.SqlDbType.NVarChar)).Value = user.CustomerDetails;

                                com.Parameters.Add(new SqlParameter("LOGIN_NAME", System.Data.SqlDbType.NVarChar, 60)).Value = user.LoginName;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("EMAIL", System.Data.SqlDbType.VarChar, 256)).Value = user.Email;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("PASS", System.Data.SqlDbType.NVarChar, 60)).Value = user.Password;
                                com.Parameters.Add(new SqlParameter("CREATION_TIME", System.Data.SqlDbType.DateTime)).Value = user.ValidFrom;
                                //com.Parameters.Add(new SqlParameter("LINK", System.Data.SqlDbType.VarChar)).Value = user.Link;
                                //com.Parameters.Add(new SqlParameter("VALID_TO", System.Data.SqlDbType.DateTime)).Value = user.ValidTo;
                                com.Parameters.Add(new SqlParameter("KEY", System.Data.SqlDbType.UniqueIdentifier)).Value = user.Key;

                                break;
                            case TUserRole.Agent:
                                com.CommandText = "spUserAddAdmin";
                                com.Parameters.Add(new SqlParameter("AGENT_NAME", System.Data.SqlDbType.NVarChar, 100)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("LOGIN_NAME", System.Data.SqlDbType.NVarChar, 60)).Value = user.LoginName;
                                com.Parameters.Add(new SqlParameter("NAME", System.Data.SqlDbType.NVarChar, 80)).Value = user.Name;
                                com.Parameters.Add(new SqlParameter("EMAIL", System.Data.SqlDbType.VarChar, 256)).Value = user.Email;
                                com.Parameters.Add(new SqlParameter("PHONE", System.Data.SqlDbType.NVarChar, 60)).Value = user.Phone;
                                com.Parameters.Add(new SqlParameter("PASS", System.Data.SqlDbType.NVarChar, 60)).Value = user.Password;
                                com.Parameters.Add(new SqlParameter("CREATION_TIME", System.Data.SqlDbType.DateTime)).Value = user.ValidFrom;
                                com.Parameters.Add(new SqlParameter("LINK", System.Data.SqlDbType.VarChar)).Value = user.Link;
                                com.Parameters.Add(new SqlParameter("VALID_TO", System.Data.SqlDbType.DateTime)).Value = user.ValidTo;
                                com.Parameters.Add(new SqlParameter("KEY", System.Data.SqlDbType.UniqueIdentifier)).Value = user.Key;
                                break;
                        }
                    }

                    try
                    {
                        int value = (int)com.ExecuteScalarWithRetry();
                        if (value > 0)
                        {
                            t.Commit();

                            //only when creation of a new user
                            if (user.ID <= 0)
                            {
                                user.ID = value;

                                //no need to create blob containers as they will be created with the first blob upload
                                //need to send an email to validate the link
                                SendActivationEmail(con, out Message);
                            }

                            result = true;
                        }
                        else if (value == -5)
                            Message += "Wybrana nazwa użytkownika już istnieje" + Program.separator;
                        else if (value == -10)
                            Message += "Wybrany email użytkownika już istnieje" + Program.separator;
                        else
                            Message += string.Format("Nieznany błąd: {0}", value) + Program.separator;
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        Message += e.Message + Program.separator;
                    }
                }
            }
            return result;
        }

        void SendActivationEmail(ReliableSqlConnection con, out string Message)
        {
            //todo generate body 
            string body = "Witamy w naszym serwisie. Poniżej znajdziesz kod który musisz wpisać przy pierwszym logowaniu celem aktywowania konta.<br/>Kod: " + user.Link;
            string subject = "Ubezpieczarka - witaj w serwisie";
            EmailManager.SendEmail(con, user.ID, body, subject, out Message);
        }

        bool IsValid(out string Message)
        {
            if (!base.Validate((object)this.user, out Message))
                return false;

            if (user.ID <= 0)
            {
                //make sure login name is not empty
                if (string.IsNullOrEmpty(this.user.LoginName))
                    Message += "Błędna nazwa użytkownika. Brak wartości." + Program.separator;

                if (this.user.LoginName.Length > 60)
                    Message += "Błędna nazwa użytkownika. Przekroczono wymaganą długość: 60 znaków." + Program.separator;


                //email
                if (string.IsNullOrEmpty(this.user.Email))
                    Message += "Błędny email. Brak wartości" + Program.separator;
                else
                {
                    try
                    {
                        System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(this.user.Email);
                    }
                    catch (Exception e)
                    {
                        Message += string.Format("Błędny email. {0}", e.Message) + Program.separator;
                    }
                }

                if (this.user.Email.Length > 256)
                    Message += "Błędny email. Przekroczono wymaganą długość: 256 znaków." + Program.separator;

                //pass
                string message;
                if (!CheckPasswordStrength(this.user.Pass, 8, 128, 1, 1, 1, 1, out message))
                    Message += Program.separator + message;

                if (this.user.Pass != this.user.PassConf)
                    Message += "Hasło i potwierdzenie hasła nie są identyczne. Upewnij się że wprowadzono poprawne i identyczne wartości" + Program.separator;
            }


            //name
            if (string.IsNullOrEmpty(this.user.Name))
                Message += "Błędne imię i nazwisko. Brak wartości" + Program.separator;

            if (this.user.Name.Length > 80)
                Message += "Błędne imię i nazwisko. Przekroczono wymaganą długość: 80 znaków." + Program.separator;

            //phone
            if (!string.IsNullOrEmpty(this.user.Phone))
            {
                if (this.user.Phone.Length > 20)
                    Message += "Błędny telefon. Przekroczono wymaganą długość: 20 znaków." + Program.separator;
            }

            return string.IsNullOrEmpty(Message);
        }

        static bool CheckPasswordStrength(string Password, int MinimumLength, int MaximumLength, int MinimumCapitals, int MinimumNumbers, int MinimumSpecialCharacters,
           int MinimumLowerCase, out string ErrorString)
        {
            string Errors = "";
            bool passed;
            string PasswordRegex = "^" +
                ((MinimumCapitals > 0)
                    ? "(?=" + string.Concat(Enumerable.Repeat(".*[A-Z]", MinimumCapitals)) + ")"
                    : "") +
                ((MinimumSpecialCharacters > 0)
                    ? "(?=" +
                        string.Concat(Enumerable.Repeat(".*[!@#$&*£^]", MinimumSpecialCharacters)) +
                        ")"
                    : "") +
                ((MinimumNumbers > 0)
                    ? "(?=" + string.Concat(Enumerable.Repeat(".*[0-9]", MinimumNumbers)) + ")"
                    : "") +
                ((MinimumLowerCase > 0)
                    ? "(?=" + string.Concat(Enumerable.Repeat(".*[a-z]", MinimumLowerCase)) + ")"
                    : "") +
                ".{" + MinimumLength.ToString() + "," + MaximumLength.ToString() + "}$";
            Regex StrongPassword = new Regex(PasswordRegex);
            passed = StrongPassword.Match(Password).Success;

            if (!passed)
            {
                Errors = "Hasło musi mieć";
                if (Password.Length < MinimumLength)
                {
                    Errors += " przynajmniej " + MinimumLength + " znaki" + Program.separator;
                }
                if (Password.Length > MaximumLength)
                {
                    Errors += " maksymalnie " + MaximumLength + " znaki" + Program.separator;
                    ErrorString = Errors;
                    return passed;
                }
                if (MinimumCapitals > 0)
                {
                    string test = "^" + "(?=" + string.Concat(Enumerable.Repeat(".*[A-Z]", MinimumCapitals)) +
                                  @")[A-Za-z\d$@$!%*#?&]{1," + MaximumLength.ToString() + "}$";
                    Regex CheckCapitals = new Regex(test);
                    if (!CheckCapitals.Match(Password).Success)
                    {
                        Errors += " przynajmniej " + MinimumCapitals + " duże listery" + Program.separator;
                    }
                }
                if (MinimumSpecialCharacters > 0)
                {
                    Regex CheckSpecialCharacters = new Regex("^" + "(?=" + string.Concat(Enumerable.Repeat(".*[!@#$&*£^]", MinimumSpecialCharacters)) +
                                          @")[A-Za-z\d$@$!%*#?&]{1," + MaximumLength.ToString() + "}$");
                    if (!CheckSpecialCharacters.Match(Password).Success)
                    {
                        Errors += " przynajmniej " + MinimumSpecialCharacters + " znaki specjalne (!@#$&*£^)" + Program.separator;
                    }
                }
                if (MinimumNumbers > 0)
                {
                    Regex CheckNumbers = new Regex("^" + "(?=" + string.Concat(Enumerable.Repeat(".*[0-9]", MinimumNumbers)) +
                                           @")[A-Za-z\d$@$!%*#?&]{1," + MaximumLength.ToString() + "}$");
                    if (!CheckNumbers.Match(Password).Success)
                    {
                        Errors += " przynajmniej " + MinimumNumbers + " cyfry" + Program.separator;
                    }
                }
                if (MinimumLowerCase > 0)
                {
                    Regex CheckLowerCase = new Regex("^" + "(?=" + string.Concat(Enumerable.Repeat(".*[a-z]", MinimumLowerCase)) +
                                           @")[A-Za-z\d$@$!%*#?&]{1," + MaximumLength.ToString() + "}$");
                    if (!CheckLowerCase.Match(Password).Success)
                    {
                        Errors += " przynajmniej " + MinimumLowerCase + " małe listery" + Program.separator;
                    }
                }
            }

            ErrorString = Errors;
            return passed;

        }

    }
}