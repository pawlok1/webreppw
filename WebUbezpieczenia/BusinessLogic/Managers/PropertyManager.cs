﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations.Model;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Helpers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;
using WebUbezpieczenia.Models;

namespace WebUbezpieczenia.BusinessLogic.Managers
{
    class PropertyManager : BaseClassDbAccess, IBaseManager
    {
        private ObjectProperty property;

        //should not pass property when showing a list
        public PropertyManager(ObjectProperty Property = null)
        {
            if (Property != null)
                this.property = Property;
        }

        public List<SelectListItem> GetListItem(DataSet ds)
        {
            List<SelectListItem> wyn = new List<SelectListItem>();
            foreach(DataRow dr in ds.Tables[0].Rows)
            {
                wyn.Add(new SelectListItem() { Text = dr["NAME"].ToString(), Value = dr["ID"].ToString() });
            }
            return wyn;
        }

        public void LoadDictionaryData(ReliableSqlConnection con, DataSet FuleType, DataSet ParkingPlace)
        {
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spDictionaryList";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("GROUP_ID", System.Data.SqlDbType.SmallInt)).Value = TDictionaryGroup.FuelType;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(FuleType);
            }
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spDictionaryList";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("GROUP_ID", System.Data.SqlDbType.SmallInt)).Value = TDictionaryGroup.ParkingPlace;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(ParkingPlace);
            }

        }

        public IEnumerable<PropertyListModels> LoadPropertyList(ReliableSqlConnection con, int CustomerID)
        {
            List<PropertyListModels> ret = new List<PropertyListModels>();
            DataSet result = new DataSet("property");
            using (var com = con.CreateCommand())
            {
                com.CommandText = "spCustomerPropertyList";
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;
                com.Parameters.Add(new SqlParameter("CUSTOMER_ID", System.Data.SqlDbType.Int)).Value = CustomerID;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(result);
                
                foreach(DataRow dr in result.Tables[0].Rows)
                {
                    PropertyListModels pm = new PropertyListModels();
                    
                    pm.Nazwa = dr[1].ToString();
                    pm.Rejestracja = dr[2].ToString();
                    pm.Rodzaj = dr[4].ToString();
                    pm.PropertyID = int.Parse(dr[0].ToString());
                    ret.Add(pm);
                }
            }
            return ret;
        }

        public bool LoadProperty(ReliableSqlConnection con, ObjectProperty o, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                using (var com = con.CreateCommand())
                {
                    com.CommandText = "spPropertyGet";
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("PROPERTY_ID", SqlDbType.Int)).Value = o.ID;

                    using (var r = com.ExecuteReaderWithRetry())
                    {
                        if (r.HasRows)
                        {
                            while (r.Read())
                            {
                                o.Name = DBHelper.ToString("PROPERTY_NAME", r);
                                o.PropertyTypeID = DBHelper.ToInt16("PROPERTY_TYPE_ID", r);
                                o.RegNumber = DBHelper.ToString("CAR_REG_NUMBER", r);
                                if (o.PropertyTypeID == 1)
                                    o.CarDetails = DBHelper.ToString("PROPERTY_DATA", r);
                                else if (o.PropertyTypeID == 2)
                                    o.BuildingDetails = DBHelper.ToString("PROPERTY_DATA", r);
                            }
                            result = true;
                        }
                        else
                            Message += "Obiekt nie został znaleziony";
                    }
                }
            }
            catch (Exception e)
            {
                Message += e.Message + Program.separator;
            }

            return result;
        }

        public bool Create(out string Message)
        {
            if (!IsValid(out Message))
                return false;

            bool result = false;

            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                using (var com = con.CreateCommand())
                {
                    SqlTransaction t = (SqlTransaction)con.BeginTransaction();
                    com.Transaction = t;
                    if (property.ID > 0)
                    {
                        com.CommandText = "spPropertyUpdate";
                        com.Parameters.Add(new SqlParameter("ID", SqlDbType.Int)).Value = property.ID;
                    }
                    else
                    {
                        com.CommandText = "spPropertyAdd";
                        com.Parameters.Add(new SqlParameter("CUSTOMER_ID", SqlDbType.Int)).Value = property.UserID;
                        com.Parameters.Add(new SqlParameter("PROPERTY_TYPE_ID", SqlDbType.SmallInt)).Value = property.PropertyTypeID;
                    }
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                    com.Parameters.Add(new SqlParameter("NAME", SqlDbType.NVarChar, 80)).Value = property.Name;
                    com.Parameters.Add(new SqlParameter("CAR_REG_NUMBER", SqlDbType.VarChar, 12)).Value = property.RegNumber;
                    if (property.PropertyTypeID == 1)
                        com.Parameters.Add(new SqlParameter("PROPERTY_DATA", SqlDbType.NVarChar)).Value = property.CarDetails;
                    else if (property.PropertyTypeID == 2)
                        com.Parameters.Add(new SqlParameter("PROPERTY_DATA", SqlDbType.NVarChar)).Value = property.BuildingDetails;

                    try
                    {
                        int value = (int)com.ExecuteScalarWithRetry();
                        if (value > 0)
                        {
                            t.Commit();
                            property.ID = value;

                            result = true;
                        }
                        else if (value == -1)
                            Message += "Brak dostępu";
                        else
                            Message += string.Format("Nieznany błąd: {0}", value);
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        Message += e.Message + Program.separator;
                    }
                }
            }
            return result;
        }

        bool IsValid(out string Message)
        {
            if (!base.Validate((object)this.property, out Message))
                return false;

            //userID
            if (this.property.UserID <= 0)
                Message += "Błędny klient. Brak wartości" + Program.separator;

            //name
            if (string.IsNullOrEmpty(this.property.Name))
                Message += "Błędna nazwa. Brak wartości." + Program.separator;

            if (this.property.Name.Length > 80)
                Message += "Błędna nazwa. Przekroczono wymaganą długość: 80 znaków." + Program.separator;

            //PropertyTypeID
            if (this.property.PropertyTypeID <= 0)
                Message += "Błędny typ obiektu lub typ nie został wybrany. Brak wartości" + Program.separator;

            return string.IsNullOrEmpty(Message);
        }

        public int LoadPropertyType(ReliableSqlConnection con, List<SelectListItem> Items, Int16 SelectedValue = -1)
        {
            return LoadComboBox(con, Items, SelectedValue, "spPropertyTypeList");
        }
    }

}