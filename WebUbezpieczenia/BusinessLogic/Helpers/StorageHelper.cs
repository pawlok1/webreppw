﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Objects;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    public class StorageHelper
    {
        static string formatStrCustomer = @"{0}/{1}/{2}";
        static string formatStrCustomerProperty = @"{0}/{1}/{2}/{3}";
        static string storageConn = "defaultStorageConn";
        static string storageContainer = "data";
        static CloudStorageAccount storageAcc = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings[storageConn].ConnectionString);
        static CloudBlobClient client = storageAcc.CreateCloudBlobClient();
        static CloudBlobContainer container = client.GetContainerReference(storageContainer);

        /*
        public void AddFile(String FileContent, Guid UserKey, Guid CustomerKey, String FileName, int PropertyID = 0)
        {            
            string filePath = string.Format(formatStrCustomer, UserKey, CustomerKey, FileName);
            if (PropertyID>0)
                filePath = string.Format(formatStrCustomerProperty, UserKey, CustomerKey, PropertyID, FileName);

            CloudBlockBlob blob = container.GetBlockBlobReference(filePath);
            byte[] byteArray = Encoding.UTF8.GetBytes(FileContent);
            using (var stream = new MemoryStream(byteArray))
            {
                blob.UploadFromStream(stream);
            }
        }
        */

        public void AddFile(TDocumentOwner Owner, Stream FileStream, Guid UserKey, Guid CustomerKey, String FileName, int ID)
        {
            string filePath = string.Format(formatStrCustomer, UserKey, CustomerKey, FileName);
            switch (Owner)
            {
                case TDocumentOwner.Policy:
                    filePath = string.Format(formatStrCustomerProperty, UserKey, CustomerKey, ID, FileName);
                    break;
            }
            CloudBlockBlob blob = container.GetBlockBlobReference(filePath);
            blob.UploadFromStream(FileStream);
        }

        public void DeleteBlob(TDocumentOwner Owner, Guid UserKey, Guid CustomerKey, String FileName, int ID)
        {
            string filePath = string.Format(formatStrCustomer, UserKey, CustomerKey, FileName);
            switch (Owner)
            {
                case TDocumentOwner.Policy:
                    filePath = string.Format(formatStrCustomerProperty, UserKey, CustomerKey, ID, FileName);
                    break;
            }

            CloudBlockBlob blob = container.GetBlockBlobReference(filePath);
            blob.DeleteIfExists();
        }
    }
}