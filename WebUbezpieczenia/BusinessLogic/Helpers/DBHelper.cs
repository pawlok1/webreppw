﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    static class DBHelper
    {
        static public Guid ToGuid(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetGuid(FieldID);
            else return Guid.Empty;
        }

        static public Guid ToGuid(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return Guid.Empty;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return Guid.Empty;
                else
                    return (Guid)r[FieldName];
            }
        }

        static public DateTime ToDate(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetDateTime(FieldID);
            else return DateTime.MinValue;
        }

        static public DateTime ToDate(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return DateTime.MinValue;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return DateTime.MinValue;
                else
                    return (DateTime)r[FieldName];
            }
        }

        static public String ToString(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetString(FieldID);
            else return "";
        }

        static public String ToString(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return "";
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return "";
                else
                    return (String)r[FieldName];
            }
        }

        static public Int64 ToInt64(Int32 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetInt64(FieldID);
            else return 0;
        }

        static public Int64 ToInt64(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Int64)r[FieldName];
            }
        }

        static public Int32 ToInt(Int32 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetInt32(FieldID);
            else return 0;
        }

        static public Int32 ToIntPercent(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return Convert.ToInt32(((Int32)r[FieldName] * 73.0) / 100.0);
            }
        }

        static public Int32 AsPercentage(Int32 value, Int32 layoutWidth)
        {
            if (layoutWidth != 1000)
                return value;
            else
                return Convert.ToInt32(value * 100.0 / 73.00);
        }

        static public Int32 ToInt(String FieldName, SqlDataReader r, Int32 NullValue = 0)
        {

            if (r[FieldName] == null)
                return NullValue;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return NullValue;
                else
                {
                    return (Int32)r[FieldName];
                }
            }
        }

        static public Decimal ToCurr(Int32 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetDecimal(FieldID);
            else return 0;
        }

        static public Decimal ToCurr(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Decimal)r[FieldName];
            }
        }

        static public Byte ToInt8(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetByte(FieldID);
            else return 0;
        }

        static public Byte ToInt8(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Byte)r[FieldName];
            }
        }

        static public Int16 ToInt16(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetInt16(FieldID);
            else return 0;
        }

        static public Int16 ToInt16(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Int16)r[FieldName];
            }
        }

        static public Decimal ToDecimal(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetDecimal(FieldID);
            else return 0;
        }


        static public Decimal ToDecimal(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Decimal)r[FieldName];
            }
        }



        static public Double ToDouble(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetDouble(FieldID);
            else return 0;
        }

        static public Double ToDouble(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return 0;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return 0;
                else
                    return (Double)r[FieldName];
            }
        }

        static public Double ToDouble(double? r)
        {
            if (r != null)
                return (Double)r;
            else return 0;
        }

        static public Boolean ToBool(Int16 FieldID, SqlDataReader r)
        {
            if (!(r.IsDBNull(FieldID)))
                return r.GetBoolean(FieldID);
            else return false;
        }

        static public Boolean ToBool(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return false;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return false;
                else
                    return (Boolean)r[FieldName];
            }
        }

        static public Boolean ToBoolNullAsTrue(String FieldName, SqlDataReader r)
        {
            if (r[FieldName] == null)
                return true;
            else
            {
                if (r[FieldName] == DBNull.Value)
                    return true;
                else
                    return (Boolean)r[FieldName];
            }
        }
    }
}