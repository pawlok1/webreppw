﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Objects;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    public class TmpSessionClass
    {
        public int userID { get; set; }
        public  int agentID { get; set; }
        public  TUserRole roleID { get; set; }

        public  string userName { get; set; }
        public  string userEmail { get; set; }

        public  Guid userKey { get; set; }

    }
    public static class SessionHelper
    {
        public static int userID;
        public static int agentID;
        public static TUserRole roleID;

        public static string userName;
        public static string userEmail;

        public static Guid userKey;


        /*PC only*/
        public static ObjectUser user;

        public static void ClearSession()
        {
            agentID = -1;
            userID = -1;
            roleID = TUserRole.Unknown;

            userName = string.Empty;
            userEmail = string.Empty;

            userKey = Guid.Empty;
        }

        public static String toString()
        {
            TmpSessionClass o = new TmpSessionClass() { userID = userID, agentID = agentID, roleID = roleID, userName = userName, userEmail = userEmail, userKey = userKey };
            string json = JsonConvert.SerializeObject(o);
            return json;
        }

        public static void fromString(string s)
        {                        
            TmpSessionClass otmp = JsonConvert.DeserializeObject<TmpSessionClass>(s);
            userID = otmp.userID;
            agentID = otmp.agentID;
            roleID = otmp.roleID;
            userName = otmp.userName;
            userEmail = otmp.userEmail;
            userKey = otmp.userKey;
        }

        public static void Load(int ID, SqlDataReader r)
        {
            SessionHelper.userID = ID;
            SessionHelper.agentID = DBHelper.ToInt("AGENT_ID", r);
            SessionHelper.roleID = (TUserRole)DBHelper.ToInt("ROLE_ID", r);

            SessionHelper.userName = DBHelper.ToString("USER_NAME", r);
            SessionHelper.userEmail = DBHelper.ToString("EMAIL", r);

            SessionHelper.userKey = DBHelper.ToGuid("KEY", r);
        }
    }
}