﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    public static class ActivationLinkHelper
    {
        private static string webLinkUrlFormat = "Login.aspx?v={0}{1}{2}{3}{4}";
        private static string linkUrlFormat = "{0}";

        public static string Generate()
        {
            string result = string.Format(ActivationLinkHelper.linkUrlFormat,
                Guid.NewGuid());

            return result;
        }

        public static string GenerateWebLink()
        {
            string result = string.Format(ActivationLinkHelper.webLinkUrlFormat,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid());

            return result;
        }
    }
}