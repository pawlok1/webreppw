﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    public class StorageDocumentHelper
    {
        /*
        public DataSet Add(TDocumentOwner Owner, string FileDescription, Guid CustomerKey, int ID, DataGridView Grid, out string Message)
        {
            DataSet result = null;
            Message = string.Empty;
            OpenFileDialog d = new OpenFileDialog();
            //d.Multiselect = true;
            d.CheckPathExists = true;
            DialogResult res = d.ShowDialog();
            if (DialogResult.OK == res)
            {
                if (AddDocumentList(Owner, d.FileNames, FileDescription, CustomerKey, ID, out Message))
                {
                    //update list
                    using (var con = ConnectionManager.GetConnection())
                    {
                        con.Open();
                        switch (Owner)
                        {
                            case TDocumentOwner.Customer:
                                result = LoadDocumentsList(con, Owner, ID);
                                break;
                            case TDocumentOwner.Policy:
                                result = LoadDocumentsList(con, Owner, ID);
                                break;
                            default:
                                throw new Exception("Niewłaściwy rodzaj kontenera dokumentów");
                        }
                    }
                }
            }
            return result;
        }
        */
        public bool Edit(ReliableSqlConnection con, TDocumentOwner Owner, int DocumentID, string Value, out string Message)
        {
            Message = string.Empty;
            using (var com = con.CreateCommand())
            {
                switch (Owner)
                {
                    case TDocumentOwner.Customer:
                        com.CommandText = "spPolicyDocumentUpdate";
                        break;
                    case TDocumentOwner.Policy:
                        com.CommandText = "spPolicyDocumentUpdate";
                        break;
                    default:
                        throw new Exception("Niewłaściwy rodzaj kontenera dokumentów");
                }
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("DOCUMENT_ID", SqlDbType.Int)).Value = DocumentID;
                com.Parameters.Add(new SqlParameter("NAME", SqlDbType.NVarChar, 30)).Value = Value;
                com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                int value = (int)com.ExecuteScalarWithRetry();
                return (value > 0);
            }
        }

        public bool Delete(ReliableSqlConnection con, TDocumentOwner Owner, int DocumentID, out string Message)
        {
            Message = string.Empty;

            //StorageHelper h = new StorageHelper();
            //if (h.DeleteBlob)

            using (var com = con.CreateCommand())
            {
                switch (Owner)
                {
                    case TDocumentOwner.Customer:
                        com.CommandText = "spCustomerDocumentDelete";
                        break;
                    case TDocumentOwner.Policy:
                        com.CommandText = "spPolicyDocumentDelete";
                        com.Parameters.Add(new SqlParameter("DELETION_DATE", SqlDbType.DateTime)).Value = DateTime.Now;
                        break;
                    default:
                        throw new Exception("Niewłaściwy rodzaj kontenera dokumentów");
                }
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("DOCUMENT_ID", SqlDbType.Int)).Value = DocumentID;
                com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                int value = (int)com.ExecuteScalarWithRetry();
                return (value > 0);
            }
        }

        private bool AddDocumentList(TDocumentOwner Owner, string[] Documents, string FileDescription, Guid CustomerKey, int ID, out string Message)
        {
            Message = string.Empty;

            StorageHelper h = new StorageHelper();
            using (var con = ConnectionManager.GetConnection())
            {
                con.Open();
                foreach (var item in Documents)
                {
                    string fileName = Path.GetFileName(item);
                    if (fileName.Length > 101)
                    {
                        Message += string.Format("Nazwa pliku: {0} jest za długa. Maksymalna dóugość to 101 znaków.", fileName) + Program.separator;
                        continue;
                    }

                    using (var r = new FileStream(item, FileMode.Open, FileAccess.Read))
                    {
                        try
                        {
                            string storageFileName = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + fileName;
                            h.AddFile(Owner, r, SessionHelper.userKey, CustomerKey, storageFileName, ID);

                            using (var com = con.CreateCommand())
                            {
                                switch (Owner)
                                {
                                    case TDocumentOwner.Customer:
                                        com.CommandText = "spCustomerDocumentAdd";
                                        com.Parameters.Add(new SqlParameter("CUSTOMER_ID", SqlDbType.Int)).Value = ID;
                                        break;
                                    case TDocumentOwner.Policy:
                                        com.CommandText = "spPolicyDocumentAdd";
                                        com.Parameters.Add(new SqlParameter("POLICY_ID", SqlDbType.Int)).Value = ID;
                                        break;
                                    default:
                                        throw new Exception("Niewłaściwy rodzaj kontenera dokumentów");
                                }
                                com.CommandType = CommandType.StoredProcedure;
                                com.Parameters.Add(new SqlParameter("USER_ID", SqlDbType.Int)).Value = SessionHelper.userID;
                                if (string.IsNullOrEmpty(FileDescription))
                                    com.Parameters.Add(new SqlParameter("NAME", SqlDbType.NVarChar, 30)).Value = "";
                                else
                                    com.Parameters.Add(new SqlParameter("NAME", SqlDbType.NVarChar, 30)).Value = FileDescription;
                                com.Parameters.Add(new SqlParameter("FILE_NAME", SqlDbType.NVarChar, 256)).Value = fileName;
                                com.Parameters.Add(new SqlParameter("STORAGE_FILE_NAME", SqlDbType.NVarChar, 256)).Value = storageFileName;
                                com.ExecuteNonQueryWithRetry();
                            }
                        }
                        catch (Exception e)
                        {
                            Message += e.Message + Program.separator;
                        }
                    }
                }
            }
            return true;
        }

        public DataSet LoadDocumentsList(ReliableSqlConnection con, TDocumentOwner Owner, int ID)
        {
            DataSet result = new DataSet("documents");

            using (var com = con.CreateCommand())
            {
                switch (Owner)
                {
                    case TDocumentOwner.Customer:
                        com.CommandText = "spCustomerDocumentList";
                        com.Parameters.Add(new SqlParameter("CUSTOMER_ID", System.Data.SqlDbType.Int)).Value = ID;
                        break;
                    case TDocumentOwner.Policy:
                        com.CommandText = "spPolicyDocumentList";
                        com.Parameters.Add(new SqlParameter("POLICY_ID", System.Data.SqlDbType.Int)).Value = ID;
                        break;
                    default:
                        break;
                }
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add(new SqlParameter("USER_ID", System.Data.SqlDbType.Int)).Value = SessionHelper.userID;
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(result);
            }
            return result;
        }
    }
}