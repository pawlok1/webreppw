﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Helpers
{
    public class QueueHelper
    {
        static string storageConn = "defaultStorageConn";
        static CloudStorageAccount storageAcc = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings[storageConn].ConnectionString);
        static CloudQueueClient queueClient = storageAcc.CreateCloudQueueClient();

        public void EmailAddToQueue(int ID)
        {
            AddToQueue(ID.ToString(), "emails", 0, 45);
        }

        private void AddToQueue(string Message, string Queue, int Minutes, int Secounds)
        {
            var queue = queueClient.GetQueueReference(Queue);
            //if (queue.Exists())
            //{
            var message = new CloudQueueMessage(Message);
            //set execution time to 5min
            var options = new QueueRequestOptions();
            options.ServerTimeout = new TimeSpan(0, Minutes, Secounds);
            queue.AddMessage(message, null, new TimeSpan(0, 0, 10), options);
            //}
        }
    }
}