﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Helpers;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectUserLogin
    {
        public ObjectUserLogin(string Name, string Pass)
        {
            this.name = Name;
            EncriptionHelper h = new EncriptionHelper();
            //if (!string.IsNullOrEmpty(Pass))
            //    this.password = h.EncryptString(Pass);
            this.password = Pass;
        }

        public int ID;

        //name or email
        string name;
        public string Name { get { return name; } }

        string password;
        public string Password { get { return password; } }

        public void ClearPassword()
        {
            password = string.Empty;
        }
    }
}