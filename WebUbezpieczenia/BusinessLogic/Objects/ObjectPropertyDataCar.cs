﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectPropertyDataCar
    {
        public string Fuel;
        public string Body;
        public string ParkingLocation;
        public bool FromAbroad;
        public int NumberOfOwners;
        public string Protection;
        public string PreviousInsurer;
        public int NumberOfCars;
    }
}