﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectInstalment
    {
        public int ID { get; set; }
        public short Number { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool Paid { get; set; }
        public string Notes { get; set; }
    }
}