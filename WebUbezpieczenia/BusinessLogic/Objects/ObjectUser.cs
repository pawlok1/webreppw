﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUbezpieczenia.BusinessLogic.Helpers;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectUser
    {
        public ObjectUser(bool GenateActivationLink)
        {
            this.ValidFrom = DateTime.Now.ToUniversalTime();
            if (GenateActivationLink)
            {
                //this.ValidFrom = DateTime.Now.ToUniversalTime();
                this.ValidTo = ValidFrom.AddDays(1).ToUniversalTime();
                link = ActivationLinkHelper.Generate();
                weblink = ActivationLinkHelper.GenerateWebLink();
            }

            key = Guid.Empty;
        }

        public int ID;
        public string LoginName;
        public string Name;
        public string Email;
        public string Phone;
        public bool Enabled;
        public TUserRole RoleID;
        public string Pass;
        public string PassConf;

        Guid key;
        public Guid Key { get { if (key == Guid.Empty) return Guid.NewGuid(); else return key; } set { key = value; } }

        public DateTime ValidFrom;
        public DateTime ValidTo;

        private string weblink;
        public string WebLink { get { return weblink; } }

        private string link;
        public string Link { get { return link; } }

        private string password;
        public string Password
        {
            get
            {
                if (string.IsNullOrEmpty(password))
                {
                    EncriptionHelper h = new EncriptionHelper();
                    password = h.EncryptString(Pass);
                }
                return password;
            }
        }

        //-------customer---------
        public Int16 CustomerTypeID;
        public string CustomerPostCode;
        public string CustomerCity;
        public string CustomerStreet;
        public ObjectUserData CustomerData;

        public string CustomerDetails
        {
            get
            {
                if (CustomerData == null)
                    CustomerData = new ObjectUserData();

                return JsonConvert.SerializeObject(CustomerData);
            }

            set
            {
                CustomerData = (ObjectUserData)JsonConvert.DeserializeObject(value, typeof(ObjectUserData));
            }
        }
    }
}