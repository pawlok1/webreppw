﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectProperty
    {
        public ObjectProperty(int ID = -1)
        {
            this.ID = ID;
            if (this.ID == -1)
            {
                CarDetails = string.Empty;
                BuildingDetails = string.Empty;
            }
        }

        public int ID;
        public int UserID;
        public string Name;

        public short PropertyTypeID;
        public string RegNumber;

        public ObjectPropertyDataCar Car;
        public ObjectPropertyDataBuilding Building;

        public string CarDetails
        {
            get
            {
                if (Car == null)
                    Car = new ObjectPropertyDataCar();

                return JsonConvert.SerializeObject(Car);
            }

            set
            {
                Car = (ObjectPropertyDataCar)JsonConvert.DeserializeObject(value, typeof(ObjectPropertyDataCar));
                if (Car == null)
                    Car = new ObjectPropertyDataCar();

            }
        }

        public string BuildingDetails
        {
            get
            {
                if (Building == null)
                    Building = new ObjectPropertyDataBuilding();

                return JsonConvert.SerializeObject(Building);
            }

            set
            {
                Building = (ObjectPropertyDataBuilding)JsonConvert.DeserializeObject(value, typeof(ObjectPropertyDataBuilding));
                if (Building == null)
                    Building = new ObjectPropertyDataBuilding();

            }
        }
    }
}