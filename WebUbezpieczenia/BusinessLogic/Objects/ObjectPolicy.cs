﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public class ObjectPolicy
    {
        public int ID;

        public int PrevPolicyID;

        public int PropertyID;
        public string Number;
        public short InsurerID;
        public string InsurerName;
        public DateTime CreationDate = DateTime.Today;
        public DateTime StartDate;
        public DateTime EndDate;
        public decimal Instalment1 = 0;
        public decimal Instalment2 = 0;
        public decimal Instalment3 = 0;
        public decimal Instalment4 = 0;
        public decimal Instalment0 = 0;
        public decimal Cash = 0;
        public string Notes;

        public List<ObjectInstalment> Instalments;

        public Guid CustomerKey;

        public TPolicyStatus Status;

        public ObjectPolicy(TPolicyStatus Status)
        {
            this.Status = Status;
            this.Instalments = new List<ObjectInstalment>();
        }
    }


}