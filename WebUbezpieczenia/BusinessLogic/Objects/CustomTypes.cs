﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUbezpieczenia.BusinessLogic.Objects
{
    public enum TUserRole
    {
        Unknown = 0,
        User = 1,
        UserTrusted = 2,
        Customer = 3,
        CustomerWithAccount = 5,
        Agent = 4
    }

    public enum TDocumentOwner
    {
        Customer = 0,
        Policy = 1
    }

    public enum TPolicyStatus
    {
        Default,
        Renew
    }

    public enum TDictionaryGroup
    {
        FuelType = 1,
        ParkingPlace = 2
    }
}