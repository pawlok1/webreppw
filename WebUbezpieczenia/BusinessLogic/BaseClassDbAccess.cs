﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Helpers;

namespace WebUbezpieczenia.BusinessLogic
{
    public class BaseClassDbAccess
    {
        protected bool Validate(object o, out string Message)
        {
            Message = string.Empty;
            if (o == null)
            {
                Message = "Object is null";// + Program.separator;
                return false;
            }
            else
                return true;
        }

        
        protected int LoadComboBox(ReliableSqlConnection con, List<SelectListItem> Items, Int16 SelectedValue, string CommandText)
        {
            int result = -1;
            //Items.Clear();

            //customer type should be cashed as it will not changed
            if (con.State != System.Data.ConnectionState.Open)
                con.Open();

            using (var com = con.CreateCommand())
            {
                com.CommandText = CommandText;
                com.CommandType = System.Data.CommandType.StoredProcedure;
                using (var r = com.ExecuteReaderWithRetry())
                {
                    while (r.Read())
                    {
                        //var o = new ObjectDD(DBHelper.ToInt16("ID", r), DBHelper.ToString("NAME", r));
                        //Items.Add(o);
                        Items.Add(new SelectListItem() {
                            Text = DBHelper.ToString("NAME", r)
                            , Value = DBHelper.ToInt16("ID", r).ToString()
                            , Selected = ((SelectedValue > -1) && (SelectedValue == DBHelper.ToInt16("ID", r)))
                        });

                        if ((SelectedValue > -1) && (SelectedValue == DBHelper.ToInt16("ID", r)))
                            result = Items.Count - 1;
                    }
                }
            }

            if (SelectedValue == -1)
            {
                if (Items.Count > 0)
                    result = 0;

                return result;
            }
            else
                return result;
        }
        

    }
}