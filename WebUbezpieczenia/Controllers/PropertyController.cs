﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;
using WebUbezpieczenia.Models;

namespace WebUbezpieczenia.Controllers
{
    public class PropertyController : Controller
    {
        // GET: Property
        public ActionResult Index()
        {
            return View();
        }

        // GET: Property/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Property/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Property/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Property/Edit/5
        public ActionResult Edit(int id)
        {
            PropertyViewModels model = new PropertyViewModels();
            ObjectProperty o;
            using (var con = ConnectionManager.GetConnection())
            {
                var m = new BusinessLogic.Managers.PropertyManager();

                var dsFuelType = new DataSet();
                var dsParkingPlace = new DataSet();
                m.LoadDictionaryData(con, dsFuelType, dsParkingPlace);

                model.cbFuelTypeList = m.GetListItem(dsFuelType);

                model.cbParkingPlaceList = m.GetListItem(dsParkingPlace);

                if (id > 0)
                {
                    o = new ObjectProperty();
                    o.ID = id;
                    string message;
                    if (!m.LoadProperty(con, o, out message))
                    {
                        //ErrorManager.Error(message);
                        //BlockUI();
                        //return; //stop execution if error
                    }
                    else
                    {
                        model.tbName = o.Name;
                        model.tbBody = o.Car.Body;
                        model.tbCity = o.Building.City;
                        //model.tbCurrentOwner = o.Car.NumberOfOwners.ToString();
                        //tbExtraProtection.Text = o.Car.Protection;
                        int val;
                        if (int.TryParse(o.Car.Fuel, out val))
                            model.cbFuelType = val.ToString();
                        if (int.TryParse(o.Car.ParkingLocation, out val))
                            model.cbParkingPlace = val.ToString();
                        model.tbPostcode = o.Building.PostCode;
                        //model.tbPreviousInsurer = o.Car.PreviousInsurer;
                        model.tbStreet = o.Building.Street;
                        //model.tbTotalCars = o.Car.NumberOfCars.ToString();
                        //model.cbFromAbroad = o.Car.FromAbroad;
                        model.tbRegNumber = o.RegNumber;
                    }
                }
                //model.ddPropertyType = m.LoadPropertyType(con, model.ddPropertyTypeList, o.PropertyTypeID);
                model.ddPropertyTypeList = new List<SelectListItem>();
                
            }

            return View(model);
        }

        // POST: Property/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Property/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Property/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
