﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUbezpieczenia.BusinessLogic.Managers;
using WebUbezpieczenia.BusinessLogic.Objects;
using WebUbezpieczenia.Managers;
using WebUbezpieczenia.Models;

namespace WebUbezpieczenia.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            Models.CustomerViewSearchModel model = new Models.CustomerViewSearchModel();
            //model.lista = new List<ListVi>();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Models.CustomerViewSearchModel model)
        {
            CustomerManager m = new CustomerManager();
            DataSet customer = m.LoadCustomersList();
            model.lista = new List<SelectListItem>();
            foreach (DataRow item in customer.Tables[0].Rows)
            {
                
                model.lista.Add(new SelectListItem() { Text = (String)item["NAME"], Value = item["ID"].ToString() });
            }
            return View(model);
        }
        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            Models.CustomerViewModels m = new Models.CustomerViewModels();
            //List<SelectListItem> l = new List<SelectListItem>();
            //l.Add(new SelectListItem() { Text = "asdfasd", Value = "asdfs" });
            //l.Add(new SelectListItem() { Text = "xxxx", Value = "fffff" });
            //m.ddTypeList = l;
            using (var con = ConnectionManager.GetConnection())
            {
                CustomerManager mgr = new CustomerManager();
                List<SelectListItem> l = new List<SelectListItem>();
                mgr.LoadUserType(con, l);
                m.ddTypeList = l;
            }
            
            //m.ddTypeList 
            return View(m);
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Models.CustomerViewModels m)
        {
            try
            {               
                ObjectUser o;                
                switch (m.checkBox1)
                {
                    case true:
                        o = new ObjectUser(true);
                        o.RoleID = TUserRole.CustomerWithAccount;
                        o.LoginName = m.LoginName;
                        o.Email = m.Email;
                        o.Pass = m.Password;
                        o.PassConf = m.ConfirmPassword;
                        break;
                    default:
                        o = new ObjectUser(false);
                        o.RoleID = TUserRole.Customer;
                        //set random settings
                        o.LoginName = string.Format("Ra$!d_{0}%", Guid.NewGuid().ToString());
                        o.Email = string.Format("rand_{0}@noemail.com", Guid.NewGuid().ToString());
                        o.Pass = string.Format("%$2a_{0}A{1}", Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                        o.PassConf = o.Pass;

                        break;
                }
                o.Name = m.Name;
                o.Phone = m.Phone;

                o.CustomerCity = m.CustomerCity;
                o.CustomerPostCode = m.CustomerPostCode;
                o.CustomerStreet = m.CustomerStreet;

                if (o.CustomerData == null)
                    o.CustomerData = new ObjectUserData();

                o.CustomerData.DrivingLicenceStartDate = m.DrivingLicenceStartDate;
                o.CustomerData.Kids = m.Kids;
                o.CustomerData.MaritialStatus = m.MaritialStatus;
                o.CustomerData.Occupation = m.Occupation;
                o.CustomerTypeID = short.Parse(m.ddType);

                string message;
                UserManager mgr = new UserManager(o);
                if (!mgr.Create(out message))
                {
                    ModelState.AddModelError("", message);
                    using (var con = ConnectionManager.GetConnection())
                    {
                        CustomerManager mgrl = new CustomerManager();
                        List<SelectListItem> l = new List<SelectListItem>();
                        mgrl.LoadUserType(con, l);
                        m.ddTypeList = l;
                    }

                    return View(m);                
                }
                else
                {
                    if (m.cbAfterSave)
                    {
                        return RedirectToAction("Edit");
                        //customer to cache
                        //CacheManager.Cache.User = o;
                        //DialogResult = DialogResult.Yes;
                    }
                    //else
                    //DialogResult = DialogResult.OK;
                }


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id, int PropertyID = 0)
        {
            CustomerViewEditModel model = new CustomerViewEditModel();
            string errmessage;
            ObjectUser customer = new ObjectUser(false);
            customer.ID = id;
            
            model.FCustomer_Load(customer, out errmessage, PropertyID);
            return View(model);
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
