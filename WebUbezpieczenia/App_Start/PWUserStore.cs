﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebUbezpieczenia.Models;

namespace WebUbezpieczenia
{
    internal class PWUserStore<T> : IUserStore<ApplicationUser>
    {
        private ApplicationDbContext applicationDbContext;

        public PWUserStore(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public Task CreateAsync(ApplicationUser user)
        {
            //throw new NotImplementedException();
            return Task.FromResult(new Object());
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            //throw new NotImplementedException();
            ApplicationUser u = new ApplicationUser();
            return Task.FromResult(u);
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            //throw new NotImplementedException();
            ApplicationUser u = new ApplicationUser();
            return Task.FromResult(u);
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
}