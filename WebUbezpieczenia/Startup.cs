﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebUbezpieczenia.Startup))]
namespace WebUbezpieczenia
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
